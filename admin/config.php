<?php
// HTTP
define('HTTP_SERVER', 'http://testmycart-annim.rhcloud.com/admin/');
define('HTTP_CATALOG', 'http://testmycart-annim.rhcloud.com/');

// HTTPS
define('HTTPS_SERVER', 'http://testmycart-annim.rhcloud.com/admin/');
define('HTTPS_CATALOG', 'http://testmycart-annim.rhcloud.com/');

// DIR
define('DIR_APPLICATION', '/var/lib/openshift/5389af7fe0b8cd089900015b/app-root/runtime/repo/admin/');
define('DIR_SYSTEM', '/var/lib/openshift/5389af7fe0b8cd089900015b/app-root/runtime/repo/system/');
define('DIR_DATABASE', '/var/lib/openshift/5389af7fe0b8cd089900015b/app-root/runtime/repo/system/database/');
define('DIR_LANGUAGE', '/var/lib/openshift/5389af7fe0b8cd089900015b/app-root/runtime/repo/admin/language/');
define('DIR_TEMPLATE', '/var/lib/openshift/5389af7fe0b8cd089900015b/app-root/runtime/repo/admin/view/template/');
define('DIR_CONFIG', '/var/lib/openshift/5389af7fe0b8cd089900015b/app-root/runtime/repo/system/config/');
define('DIR_IMAGE', '/var/lib/openshift/5389af7fe0b8cd089900015b/app-root/runtime/repo/image/');
define('DIR_CACHE', '/var/lib/openshift/5389af7fe0b8cd089900015b/app-root/runtime/repo/system/cache/');
define('DIR_DOWNLOAD', '/var/lib/openshift/5389af7fe0b8cd089900015b/app-root/runtime/repo/download/');
define('DIR_LOGS', '/var/lib/openshift/5389af7fe0b8cd089900015b/app-root/runtime/repo/system/logs/');
define('DIR_CATALOG', '/var/lib/openshift/5389af7fe0b8cd089900015b/app-root/runtime/repo/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', '127.5.66.2:3306');
define('DB_USERNAME', 'adminqUswVyY');
define('DB_PASSWORD', 'widescreen');
define('DB_DATABASE', 'testmycart');
define('DB_PREFIX', 'toc_');
?>